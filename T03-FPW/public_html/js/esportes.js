/*Criei essas linhas de códigos não serem executadas em todas as paginas
 *Somente na pág 'Esportes'*/

$(document).ready(function () {
    /*Iniciar com texto ocultados*/
    $("#textoFut").hide();
    $("#textoBas").hide();
    $("#textoAut").hide();
    $("#textoTen").hide();

    /*Ideia geral do código:
     *Quando clicar no esporte, aparecer o texto dele
     *Mas também ter um 'status' usando + e - funcionando como (extender e recolher)
     *se ficar clicando no mesmo titulo, o texto some e aparece enquanto o status muda*/

    /*Futebol*/
    $("#tituloFut").click(function () {
        /*alternar entre sumir e aparecer o texto*/
        $("#textoFut").slideToggle("slow", function () {

            /*alterar o 'status'*/
            if ($("#tituloFut").text() === "Futebol +") {
                $("#tituloFut").text("Futebol -");
            } else if ($("#tituloFut").text() === "Futebol -") {
                $("#tituloFut").text("Futebol +");
            }

        });

        /*sumir com todos os outros texto caso tenha algum aberto
         * e garantir que apenas um seja mostrado*/
        $("#textoBas").hide();
        $("#textoAut").hide();
        $("#textoTen").hide();

        /*mudar o status dos outros títulos*/
        $("#tituloBas").text("Basquete +");
        $("#tituloAut").text("Automobilismo +");
        $("#tituloTen").text("Tênis +");
    });

    /*Basquete*/
    $("#tituloBas").click(function () {

        /*alternar entre sumir e aparecer o texto*/
        $("#textoBas").slideToggle("slow", function () {

            if ($("#tituloBas").text() === "Basquete +") {
                $("#tituloBas").text("Basquete -");
            } else if ($("#tituloBas").text() === "Basquete -") {
                $("#tituloBas").text("Basquete +");
            }

        });

        /*sumir com todos os outros texto caso tenha algum aberto
         * e garantir que apenas um seja mostrado*/
        $("#textoAut").hide();
        $("#textoTen").hide();
        $("#textoFut").hide();


        /*mudar o status dos outros títulos*/
        $("#tituloFut").text("Futebol +");
        $("#tituloAut").text("Automobilismo +");
        $("#tituloTen").text("Tênis +");
    });

    /*Automobilismo*/
    $("#tituloAut").click(function () {

        /*sumir com todos os outros texto caso tenha algum aberto
         * e garantir que apenas um seja mostrado*/
        $("#textoFut").hide();
        $("#textoBas").hide();
        $("#textoTen").hide();

        /*alternar entre sumir e aparecer o texto*/
        $("#textoAut").slideToggle("slow", function () {

            if ($("#tituloAut").text() === "Automobilismo +") {
                $("#tituloAut").text("Automobilismo -");
            } else if ($("#tituloAut").text() === "Automobilismo -") {
                $("#tituloAut").text("Automobilismo +");
            }

        });

        /*mudar o status dos outros títulos*/
        $("#tituloFut").text("Futebol +");
        $("#tituloBas").text("Basquete +");
        $("#tituloTen").text("Tênis +");
    });

    /*Tênis*/
    $("#tituloTen").click(function () {
        /*sumir com todos os outros texto caso tenha algum aberto
         * e garantir que apenas um seja mostrado*/
        $("#textoFut").hide();
        $("#textoBas").hide();
        $("#textoAut").hide();

        /*alternar entre sumir e aparecer o texto*/
        $("#textoTen").slideToggle("slow", function () {

            if ($("#tituloTen").text() === "Tênis +") {
                $("#tituloTen").text("Tênis -");
            } else if ($("#tituloTen").text() === "Tênis -") {
                $("#tituloTen").text("Tênis +");
            }

        });

        /*mudar o status dos outros títulos*/
        $("#tituloFut").text("Futebol +");
        $("#tituloBas").text("Basquete +");
        $("#tituloAut").text("Automobilismo +");
    });

});

