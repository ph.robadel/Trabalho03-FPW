//Arquivo de script jquery principal do projeto
$(document).ready(function () {
    /*Iniciar*/
    $(".noticias").hide();
    $(".scrollMenu").hide();

    $(".menu").on({
        mouseenter: function () {
            $(".scrollMenu").show();
            $(".menu img").attr("src", "Img/cancel.png");
        },
        mouseleave: function () {
            $(".scrollMenu").hide();
            $(".menu img").attr("src", "Img/menu.png");
        }
    });
    
    $(".noticiaButton").on({
        mouseenter: function () {
            $(".noticias").show();
        },
        mouseleave: function () {
            $(".noticias").hide();
        }
    });
    
});

